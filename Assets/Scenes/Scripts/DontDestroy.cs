﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Web;
              //using Facebook.Unity;
using UnityEngine;
public class DontDestroy : MonoBehaviour
{
    void Awake()
    {
        GameObject[] objs = GameObject.FindGameObjectsWithTag("music_goodanswer");
        if(objs.Length > 1)
            Destroy(this.gameObject);

        DontDestroyOnLoad(this.gameObject);
        
    }

    public AudioSource _source;
    public AudioClip sound;


    void Start()
    {
      //  FB.Init(this.OnInitComplete, this.OnHideUnity);
       // FB.AppId = "21325234";
    }
    
    private void OnInitComplete()
    {
       // Debug.Log( "Success - Check log for details");
       // Debug.Log( "Success Response: OnInitComplete Called");
     //   string logMessage = string.Format(
     //       "OnInitCompleteCalled IsLoggedIn='{0}' IsInitialized='{1}'",
           // FB.IsLoggedIn,
          //  FB.IsInitialized);
      //  Debug.Log(logMessage);
      //  if (AccessToken.CurrentAccessToken != null)
      //  {
          //  Debug.Log(AccessToken.CurrentAccessToken.ToString());
     //   }
        

      //  FB.LogAppEvent("Run");
        //FB.Loginew List<string>() {}, this.HandleResult);
        
    }
    
    
    // protected void HandleResultHandleResult(IResult result)
    // {
    //
    // }
    
    
//    protected override string QueryAccessToken(Uri returnUrl, string authorizationCode)
//    {
//        // Note: Facebook doesn't like us to url-encode the redirect_uri value
//        var builder = new UriBuilder("https://graph.facebook.com/oauth/access_token");
//        builder.AppendQueryArgument("client_id", this.appId);
//        builder.AppendQueryArgument("redirect_uri", NormalizeHexEncoding(returnUrl.GetLeftPart(UriPartial.Path)));
//        builder.AppendQueryArgument("client_secret", this.appSecret);
//        builder.AppendQueryArgument("code", authorizationCode);
//
//        using (WebClient client = new WebClient())
//        {
//            //Get Accsess  Token
//            string data = client.DownloadString(builder.Uri);
//            if (string.IsNullOrEmpty(data))
//            {
//                return null;
//            }
//
//            var parsedQueryString = HttpUtility.ParseQueryString(data);
//            return parsedQueryString["access_token"];
//        }
//    }

//    private static string NormalizeHexEncoding(string url)
//    {
//        var chars = url.ToCharArray();
//        for (int i = 0; i < chars.Length - 2; i++)
//        {
//            if (chars[i] == '%')
//            {
//                chars[i + 1] = char.ToUpperInvariant(chars[i + 1]);
//                chars[i + 2] = char.ToUpperInvariant(chars[i + 2]);
//                i += 2;
//            }
//        }
//        return new string(chars);
//    }


    // protected void HandleResult (IResult result) {
    //     if (result.Cancelled || !String.IsNullOrEmpty(result.Error)) {
    //         Debug.Log("ShareLink Error: "+result.Error);
    //     } else if (!String.IsNullOrEmpty(result.RawResult)) {
    //         // Print post identifier of the shared content
    //       //  Debug.Log(result.PostId);
    //     } else {
    //         // Share succeeded without postID
    //         Debug.Log("ShareLink success!");
    //     }
    // }
    
    private void OnHideUnity(bool isGameShown)
    {
        Debug.Log( "Success - Check log for details");
        Debug.Log("Success Response: OnHideUnity Called {0}\n"+ isGameShown);
        Debug.Log("Is game shown: " + isGameShown);
    }
    

    public void PlaySoundinGame()
    {
        Debug.Log("AudioClip sound");
        gameObject.AddComponent<AudioSource>();
        _source.clip = sound;
        _source.playOnAwake = false;
        _source.PlayOneShot(sound);
    }
    
}
