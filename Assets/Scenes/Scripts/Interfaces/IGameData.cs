using Enums;

namespace Scenes.Scripts
{
    interface IGameData
    {
        void SaveData(Prefs type, string data);
        string LoadData(Prefs type, string defaultValue = "");
       // T LoadJson<T>(string fileName);
    }
}