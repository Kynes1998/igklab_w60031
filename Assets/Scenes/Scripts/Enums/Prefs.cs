﻿namespace Enums
{
    public enum Prefs
    {
        CategoryData,
        PlayerName,
        Gender,
        Progress,
        CurrentPassage,
        CurrentPassagePosition,
        CurrentTaskNumber,
        CurrentTaskType,
        CategoryChoosed,
    }
}
