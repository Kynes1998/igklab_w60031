using System.Collections.Generic;
using System.IO;
using System.Linq;
using Enums;
//using Helpers;
//using Models.Tasks.Base;
using Newtonsoft.Json;
using UnityEngine;

namespace Scenes.Scripts
{
    public abstract class GameData : BaseRequest, IGameData
    {
        private AudioSource _source
        {
            get { return GetComponent<AudioSource>(); }
        }

        public void PlaySoundinGame(AudioClip sound)
        {
            Debug.Log("AudioClip sound");
            gameObject.AddComponent<AudioSource>();
            _source.clip = sound;
            _source.playOnAwake = false;
            _source.PlayOneShot(sound);
        }

        public void SaveData(Prefs type, string data)
        {
            PlayerPrefs.SetString(type.ToString(), data);
            PlayerPrefs.Save();
        }
        
        public void SaveData(Prefs type, int data)
        {
            PlayerPrefs.SetInt(type.ToString(), data);
            PlayerPrefs.Save();
        }

        public string LoadData(Prefs type, string defaultValue = "")
        {
            return PlayerPrefs.GetString(type.ToString(), defaultValue);
        }
        
        public int LoadDataInt(Prefs type, int defaultValue = 0)
        {
            return PlayerPrefs.GetInt(type.ToString(), defaultValue);
        }
        

        public void SaveJson<T>(T data, string fileName)
        {

        }
    }
}