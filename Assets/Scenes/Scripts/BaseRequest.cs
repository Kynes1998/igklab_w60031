using System;
using System.Collections;
using UnityEngine;
using UnityEngine.Networking;
using System.Text;

public class BaseRequest : MonoBehaviour
{
    private string ServerURL = "https://backend.yep.academy";
    
    

    private string GetUID()
    {
        return SystemInfo.deviceUniqueIdentifier.ToUpper();
    }   
    
    protected void SendEmailRequest(String email)
    {
        String bodyJsonString = "{\"email\":\""+ email + "\",\"uid\":\"" + GetUID() + "\"}";
        StartCoroutine(PostEmail(ServerURL +"/user/add", bodyJsonString,"POST"));
    }
    
    
    protected void SendProgress(String progresType, int value)   //value": 55
    {
        String bodyJsonString = "{ \"type\":\"" + progresType + "\", \"uid\":\"" + GetUID() + "\",\"value\":\"" + value + "\"}";
        StartCoroutine(UniversalURLRequest(ServerURL +"/progress/add", bodyJsonString,"POST"));
    }
    
    
        private IEnumerator PostEmail(string url, string bodyJsonString, string metod  )
        {
            var request = new UnityWebRequest(url, metod);
            byte[] bodyRaw = Encoding.UTF8.GetBytes(bodyJsonString);
            request.uploadHandler = (UploadHandler) new UploadHandlerRaw(bodyRaw);
            request.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
            request.SetRequestHeader("Content-Type", "application/json");
            yield return request.SendWebRequest();
            Debug.Log("PostEmail ::Status Code: " + request.responseCode);
            //StartCoroutine(GETURLRequest(ServerURL + "/user/send/"));
            //  StartCoroutine(UniversalURLRequest( ServerURL + "/user/send/"+  SystemInfo.deviceUniqueIdentifier.ToUpper(), bodyJsonString,"GET"));
        }
        
        protected IEnumerator GETURLRequest()
        {
            var uri = ServerURL + "/user/send/" + SystemInfo.deviceUniqueIdentifier.ToUpper();
            
            using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
            {
                // Request and wait for the desired page.
                yield return webRequest.SendWebRequest();

                string[] pages = uri.Split('/');
                int page = pages.Length - 1;

                if (webRequest.isNetworkError)
                {
                    Debug.Log(pages[page] + ": Error: " + webRequest.error);
                }
                else
                {
                    Debug.Log(pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);
                }
            }
        }
        
        private IEnumerator UniversalURLRequest(string url, string bodyJsonString, string metod )
        {
            var request = new UnityWebRequest(url, metod);
            byte[] bodyRaw = Encoding.UTF8.GetBytes(bodyJsonString);
            request.uploadHandler = (UploadHandler) new UploadHandlerRaw(bodyRaw);
            request.downloadHandler = (DownloadHandler) new DownloadHandlerBuffer();
            request.SetRequestHeader("Content-Type", "application/json");
            yield return request.SendWebRequest();
            Debug.Log("Status Code: " + request.responseCode);
        }
}
