﻿using UnityEngine;
using UnityEngine.Video;

namespace Scenes.Scripts
{
    public class HalxMimic : MonoBehaviour
    {
        public void Start()
        {
            RunMovie();
        }
        
        public VideoPlayer videoPlayer;

        private string moviePath;
        public string MovieFile;

        public void RunMovie()
        {
            
            moviePath = Application.streamingAssetsPath + "/" + MovieFile;
            videoPlayer.playOnAwake = false;
            videoPlayer.url = moviePath;
            videoPlayer.Prepare();
            videoPlayer.Play();
        }
    }
}