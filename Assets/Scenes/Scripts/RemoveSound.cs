﻿using UnityEngine;

namespace Scenes.Scripts
{
    public class RemoveSound : MonoBehaviour
    {
        #region Components
        public RectTransform SkipButton;
        public RectTransform EnterPanel;
        
         #endregion Components
         
        public void Start()
        {
            GameObject.Find("BackgroundMusic").SetActive(false);
        }
        
    }
}