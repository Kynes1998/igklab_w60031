﻿
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class ClickSound : MonoBehaviour
{
    public AudioClip sound;
    
    private Button button
    {
        get { return GetComponent<Button>(); }
    }
    
    private AudioSource _source
    {
        get { return GetComponent<AudioSource>(); }
    }

    
    void Start()
    {
        gameObject.AddComponent<AudioSource>();
        _source.clip = sound;
        _source.playOnAwake = false;
        
        button.onClick.AddListener(()=> PlaySound() );

    }

    // Update is called once per frame
    void PlaySound()
    {
        _source.PlayOneShot(sound);
    }
}
