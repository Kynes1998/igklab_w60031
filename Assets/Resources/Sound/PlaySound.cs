﻿


using UnityEngine;

public class PlaySound : MonoBehaviour
{
    
   // public AudioClip sound;

    private AudioSource _source
    {
        get { return GetComponent<AudioSource>(); }
    }

    
    public void PlaySoundinGame(AudioClip sound)
    {
       Debug.Log("AudioClip sound");
       
       gameObject.AddComponent<AudioSource>();
       _source.clip = sound;
       _source.playOnAwake = false;
       _source.PlayOneShot(sound);
      // button.onClick.AddListener(()=>  );
      //PlaySound();
    }
}
